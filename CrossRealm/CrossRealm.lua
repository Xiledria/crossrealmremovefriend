CRRI = {}
CRRI.fn_remove_friend = UnitPopup_OnClick
function CRRI.HandleRemoveFriend(self)
    if self.value == "REMOVE_FRIEND" then
        RemoveFriend(UIDROPDOWNMENU_INIT_MENU.chatTarget)
    else
        CRRI.fn_remove_friend(self)
    end
end
UnitPopup_OnClick = CRRI.HandleRemoveFriend

CRRI.fn_unit_name = UnitName
function CRRI.HandleUnitName(unit)
    local name = CRRI.fn_unit_name(unit)
    return name, nil
end

UnitName = CRRI.HandleUnitName